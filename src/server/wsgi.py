"""Entrypoint for wsgi connection."""


from .webhook_app import WebhookApp

app = WebhookApp()  # pylint: disable=invalid-name
app.setup_webhook()

flask_app = app.flask_app  # pylint: disable=invalid-name
