"""Contains webhook application class."""


import time

import flask
import telebot

from .. import config
from .. import worker


WEBHOOK_URL_BASE = "https://{0}".format(config.WEBHOOK_HOST)
WEBHOOK_URL_PATH = "/%s/" % (config.BOT_TOKEN)


class WebhookApp:
    """Application, based on webhook."""

    def __init__(self):
        self.bot = telebot.TeleBot(config.BOT_TOKEN, threaded=False)

        self.flask_app = flask.Flask(__name__)

        @self.flask_app.route(WEBHOOK_URL_PATH, methods=['POST'])
        def webhook():  # pylint: disable= unused-variable
            if flask.request.headers.get('content-type') != 'application/json':
                flask.abort(403)

            json_string = flask.request.get_data().decode('utf-8')
            worker.process_new_updates.delay(json_string)
            return ''

    def setup_webhook(self):
        """Install hook on telegram server."""

        self.bot.remove_webhook()
        time.sleep(0.1)
        self.bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH)
