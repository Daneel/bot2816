"""Contains hanlder setupper class."""


from .base_command_setuper import BaseCommandSetuper
from .commands import AntiArmy, Pnh, RandomAnswer


class CommandSetuper(BaseCommandSetuper):
    """Aggregate command hanlders for bot2816."""

    handlers = [AntiArmy, Pnh, RandomAnswer]
