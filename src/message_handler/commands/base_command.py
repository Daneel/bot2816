"""Contains base command class."""


class BaseCommand:
    """Base command hanlder class."""

    commands = None
    regexp = None
    func = None
    content_types = ['text']

    def __init__(self, bot):

        self.bot = bot

        self.filters = {
            'commands' : self.commands,
            'regexp' : self.regexp,
            'func' : self.func,
            'content_types' : self.content_types,
        }

    def process(self, message):
        """Process message."""
        raise NotImplementedError
