"""Contains RandomAnswer class"""


import random

from .base_command import BaseCommand


ANSWERS = [
    "Пошел нахуй!!!",
    "звучит интересно",
    "ктонить ходил в военкомат?",
    "полная хуйня",
    "мамку ебал",
    "где об этом можно почитать подробнее?",
    "но это не точно", "долго над этим думал?",
    "ну ты и пидор",
    "заебал хуйню писать",
    """ С точки зрения дедукции, индукции и мозговой продукции вы
    некомпетентны в этом вопросе, поскольку каждый пессимистически
    настроенный индивидуум катастрофически модифицирует абстракции
    реального субъективизма.""",
    "да",
    "нет",
    "четко",
]


class RandomAnswer(BaseCommand):
    """Make you conversation interesting."""

    def process(self, message):
        if random.randint(1, 10) == 1:
            self.bot.send_message(message.chat.id, random.choice(ANSWERS))
