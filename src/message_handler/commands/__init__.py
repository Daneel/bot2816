"""Contains bot message handlers."""


from .anti_army import AntiArmy
from .pnh import Pnh
from .random_answer import RandomAnswer
