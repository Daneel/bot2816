"""Contains class AntiArmy message handler."""

from .base_command import BaseCommand

class AntiArmy(BaseCommand):
    """Prevent army talk."""

    regexp = r".*((в|В)оен|(с|С)олд|(г|Г)оде|(п|П)овест|(а|А)рми|"\
             r"(с|С)луж|(з|З)акон|(п|П)риз(ы|о|а)?в).*"

    def process(self, message):
        self.bot.reply_to(message, "Заебал своей хуйнёй про армию!")
