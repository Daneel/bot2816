"""Contains main bot celery task."""


from celery import Celery

from .. import config


CELERY = Celery('tasks', broker=config.BROKER)


class Cache():
    """Cache bot instance."""

    def __init__(self):
        self.__bot = None

    def get_bot(self):
        """Return cached telegram bot instance."""
        if self.__bot is None:
            import telebot  # pylint: disable=import-outside-toplevel
            from ..message_handler import CommandSetuper
            self.__bot = telebot.TeleBot(config.BOT_TOKEN, threaded=False)
            CommandSetuper(self.__bot).setup()
        return self.__bot


CACHE = Cache()


@CELERY.task(ignore_result=True)
def process_new_updates(json_string):
    """Main message process function."""

    import telebot  # pylint: disable=import-outside-toplevel
    update = telebot.types.Update.de_json(json_string)
    CACHE.get_bot().process_new_updates([update])
