"""Contains bot configuration."""


import json

CONFIG_FILE_PATH = "data/config.json"


def __load():
    """Load bots config from CONFIG_FILE_PATH."""

    with open(CONFIG_FILE_PATH) as config_file:
        config = json.load(config_file)
        bot_token = config["bot_token"]
        webhook_host = config["web_hook"]["host"]
        broker = config["broker"]

    return bot_token, webhook_host, broker


BOT_TOKEN, WEBHOOK_HOST, BROKER = __load()
