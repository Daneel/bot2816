"""Contains root class of apllication, based on TeleBot.polling method."""

import time

import telebot

from .. import config
from ..message_handler.command_setuper import CommandSetuper


class PollingApp:
    """Root class of apllication, based on TeleBot.polling method."""

    def __init__(self):
        self.bot = telebot.TeleBot(config.BOT_TOKEN, threaded=True)
        CommandSetuper(self.bot).setup()

    def run(self):
        """Execute main application loop."""
        self.bot.remove_webhook()
        time.sleep(0.1)
        self.bot.polling()
