"""Entry point in polling application."""

from .polling_app import PollingApp


def main():
    """Launch polling application."""
    app = PollingApp()
    app.run()


main()
