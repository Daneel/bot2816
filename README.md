# BOT2816

Бот для telegram, призванный разнообразить вашу беседу.
Имеет следующие возможности:

 - Рандомный ответ на сообщения в чате
 - Предотвращение бесед об армии
 - Посылает нахуй

## Содержание

1. [Dependencies](#Dependencies)
2. [Настройка](#Setup)
3. [Запуск](#Start)

## <a name="Dependencies"></a>Зависимости
 Приложения предоставляет docker контейнеры для запуска. Однако
для развертывания в production дополнительно необходимы:

 * RabbitMQ
 * Web сервер с uwsgi

Для упрощенного запуска достаточно использовать polling образ docker.

## <a name="Setup"></a>Настройка

Для конфигурации в докер контейнерах необходимо наличие файла
_/home/bot2816/data/config.json_
со следующим содержимым:

    {
        "bot_token": "SOME_BOT_TOKEN",
        "broker": "amqp://bot2816:test@rabbitmq/bot2816",
        "web_hook": {
            "host": "example.org"
        }
    }

В поле **bot_token** необходимо задать токен бота telegram.  
Поле **broker** необходимо сконфигурировать в соответсвии с параметрами
RABBITMQ в настройках окружения.
В случаи запуска для отладки можно оставить пустым.
Правила кофигурации строки брокера смотрите в
[документации Celery](https://docs.celeryproject.org/en/latest/userguide/configuration.html#conf-broker-settings).    
**host** в случаи запуска для отладки можно оставить пустым.

## <a name="Start"></a>Запуск

### Отладка

Для запуска в дебажном режиме достаточна запустить
**docker-compose.debug.yaml** через docker-compose.

### Production

Для запуска в production конфигурации необходимо запустить контейнеры
**worker** и **server**. Команды запусков контейнеров не
 автоматизированны для локальной настройки. Пример конфигурации docker-compose:

    bot2816_worker:
        build:
            context: .
            dockerfile: images/worker/Dockerfile
        depends_on:
            - rabbitmq
        volumes:
            - ./data/bot2816:/home/bot2816/data:ro
        command: celery -A src.worker worker

    bot2816_server:
        build:
            context: .
            dockerfile: images/server/Dockerfile
        depends_on:
            - rabbitmq
        volumes:
            - ./data/bot2816:/home/bot2816/data:ro
        command: uwsgi --socket 0.0.0.0:5000 --protocol=uwsgi -w src.server.wsgi:flask_app --master --uid bot2816

Для работы необходимо наличие RabbitMQ. А также вебсервер, который
будет посылать данные через uwsgi.
